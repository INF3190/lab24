import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BonComponent } from './bon/bon.component';
import { MoyenComponent } from './moyen/moyen.component';
import { MauvaisComponent } from './mauvais/mauvais.component';

@NgModule({
  declarations: [
    AppComponent,
    BonComponent,
    MoyenComponent,
    MauvaisComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import * as listeetudiants from '../../assets/data/etudiants.json';
@Component({
  selector: 'app-bon',
  templateUrl: './bon.component.html',
  styleUrls: ['./bon.component.css']
})
export class BonComponent implements OnInit {

  etudiants:any = (listeetudiants as any).default;
  constructor() { }

  ngOnInit(): void {
  }

  bons(){
    //this.bonsetudiants = [];
    let returnval:any[]=[];
    let i=0;
    for (let etud:any of this.etudiants){
      console.log(etud);//pour voir la trace
      if(etud.note=='A'){
      returnval[i]=etud;
      i = i +1;
      }
    }
    return (returnval);
  }

}

import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import * as listeetudiants from '../assets/data/etudiants.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'laboratoire 24';
  afficheliste:boolean = false;
  etudiants:any = (listeetudiants as any).default;

  afficher(){
    this.afficheliste = true;
  }

  cacher(){
    this.afficheliste = false;
  }
}
